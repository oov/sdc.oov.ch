package main

import (
	"bitbucket.org/oov/dgf"
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"
)

type LyricSource struct {
	No    int
	Title string
}

type LyricLine struct {
	source *dgf.SongLyric
	Source *LyricSource
	Body   string
}

type Lyric struct {
	MaxId int
	Seed  int64
	Body  []*LyricLine
	rng   *rand.Rand
	df    *dgf.Dgf
}

func (l *Lyric) String() string {
	lines := make([]string, 0, len(l.Body))
	var s string
	for _, v := range l.Body {
		if v != nil {
			s = v.Body
		} else {
			s = ""
		}
		lines = append(lines, s)
	}
	return strings.Join(lines, "\n")
}

func (l *Lyric) JSON() []byte {
	json, err := json.Marshal(l)
	if err != nil {
		panic(err)
	}
	return json
}

//乱数を元に歌詞をひとつだけ抜き出し、その歌詞から lines 行抜き出す
func (l *Lyric) selectLinesFromLyric(numLines int) []*LyricLine {
	var song *dgf.Song
	var err error
	for retry := 0; retry < 100; retry++ {
		song, err = l.df.GetSong(l.rng.Intn(l.MaxId + 1))
		if err != nil {
			continue
		}

		//歌詞をかき集める
		lyrics := make([]*dgf.SongLyric, 0)
		for _, entry := range song.Entries {
			if entry.HasLyric {
				ly, err := entry.GetLyric()
				if err != nil {
					continue
				}
				lyrics = append(lyrics, ly)
			}
		}

		//ループしても結局歌詞が見つからなかった
		if len(lyrics) == 0 {
			continue
		}

		//複数の中から歌詞を一つだけ抜き出す
		lyric := lyrics[l.rng.Intn(len(lyrics))]

		//行ごとに分解したものを更に有効行だけで分解する
		lines := make([]*LyricLine, 0)
		for _, line := range lyric.GetLines() {
			s := strings.TrimSpace(line)
			if s == "" {
				continue
			}
			lines = append(lines, &LyricLine{
				source: lyric,
				Source: &LyricSource{
					No:    lyric.GetParent().GetParent().GetIndex(),
					Title: lyric.GetParent().Title,
				},
				Body: s,
			})
		}

		//歌詞の有効行が要求されている行数の半分にも満たない場合はやり直す
		length := len(lines)
		if length*2 < numLines {
			continue
		}

		//足りない場合は適当に水増しする
		for len(lines) < numLines {
			lines = append(lines, lines[l.rng.Intn(length)])
		}

		//混ぜる
		for i := range lines {
			j := l.rng.Intn(i + 1)
			lines[i], lines[j] = lines[j], lines[i]
		}

		return lines
	}

	//本当に何もかもダメだった
	//エラーは起こさないようなつくりで行きたいのでデータを捏造する
	lines := make([]*LyricLine, 0)
	for len(lines) < numLines {
		lines = append(lines, &LyricLine{
			source: nil,
			Body:   "エラーだよぉ",
		})
	}
	return lines
}

//ランダムに歌詞を１行だけ抜き出す
func (l *Lyric) selectOneLineFromLyric() *LyricLine {
	return l.selectLinesFromLyric(1)[0]
}

func (l *Lyric) generateLyric() []*LyricLine {
	l.rng = rand.New(rand.NewSource(l.Seed))
	lines := make([]*LyricLine, 0)
	switch l.rng.Intn(6) {
	case 0:
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
	case 1:
		catch := l.selectOneLineFromLyric()
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, catch)
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, catch)
		lines = append(lines, l.selectOneLineFromLyric())
	case 2:
		catches := l.selectLinesFromLyric(2)
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, catches[0])
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, catches[1])
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, catches[0])
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, catches[1])
		lines = append(lines, l.selectOneLineFromLyric())
	case 3:
		catches := l.selectLinesFromLyric(2)
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, catches[0])
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, catches[1])
		lines = append(lines, l.selectOneLineFromLyric())
	case 4:
		catches := l.selectLinesFromLyric(2)
		lines = append(lines, catches[0])
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, catches[1])
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, nil)
		lines = append(lines, catches[0])
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, catches[1])
		lines = append(lines, l.selectOneLineFromLyric())
	case 5:
		lines = append(lines, l.selectOneLineFromLyric())
		lines = append(lines, l.selectOneLineFromLyric())
	}

	return lines
}

//歌詞を組み立てて返す
func NewLyric(df *dgf.Dgf, seed int64) *Lyric {
	maxId, err := df.GetLocalMaxId()
	if err != nil {
		panic(err)
	}
	if maxId == 0 {
		panic(fmt.Errorf("invalid local-max-id"))
	}

	lyric := &Lyric{
		MaxId: maxId,
		Seed:  seed,
		df:    df,
		rng:   rand.New(rand.NewSource(seed)),
	}

	lyric.Body = lyric.generateLyric()

	return lyric
}
