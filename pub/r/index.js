function regenerateRandomLyric(){
  if (location.search != ""){
    //パラメータが付与されている時は勘違い防止のために移動で再生成とする
    location.href = location.protocol + "//" + location.host + location.pathname;
    return;
  }

	$.get(
		'/',
		{_:new Date().getTime()},
		function(ret){
	    $('#random-lyric-container').html(ret.split('<script type="text/guide">random-lyric</script>')[1]);
      addRegenerateButton();
		},
    'text'
	);
}

function addRegenerateButton(){
	var random_lyric = $('#random-lyric:not(.suspend)');
	$.each(['before', 'after'], function(k, v){
		random_lyric[v]($('<input type="button" value="歌詞を作り直す">').click(regenerateRandomLyric));
	});
}

$(function(){
  addRegenerateButton();
});
