// +build ignore

// go run fts.go adapter.go dbfilename

package main

import (
	"bitbucket.org/oov/dgf"
	"bitbucket.org/oov/go-shellinford/shellinford"
	"bytes"
	"code.google.com/p/go.text/unicode/norm"
	"flag"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/ugorji/go/codec"
	"log"
	"os"
	"time"
)

var (
	df *dgf.Dgf
)

func main() {
	flag.Parse()

	if flag.NArg() <= 0 {
		log.Println("Usage: go run fts.go adapter.go dbfilename")
		return
	}

	db, err := leveldb.OpenFile(flag.Arg(0), &opt.Options{})
	if err != nil {
		log.Fatalln("Cannot open db:", err)
	}
	df = dgf.New(NewLevelDBAdapter(db))
	defer df.Close()

	fmi := shellinford.NewFMIndex()
	maps := make(map[int]int)

	ln, err := df.GetLocalMaxId()
	if err != nil {
		log.Fatalln("Cannot get LocalMaxId:", err)
	}

	fmiidx := 0
	size := 0
	for i := 0; i < ln; i++ {
		buf := bytes.NewBufferString("")

		song, err := df.GetSong(i)
		if err != nil {
			log.Printf("No.%d skipped: %v\n", i, err)
			continue
		}

		if i&0x7f == 0 {
			log.Printf("No.%d processing...\n", i)
		}

		for eidx, e := range song.Entries {
			if e.HasLyric {
				ly, err := e.GetLyric()
				if err != nil {
					log.Printf("Could not get No.%d Lyric #%d: %v\n", i, eidx, err)
					continue
				}

				buf.WriteString(ly.Body)
				buf.WriteByte('\n')
			}
		}

		if buf.Len() == 0 {
			continue
		}

		b := norm.NFKC.Bytes(buf.Bytes())
		fmi.Add(b)
		size += len(b)
		maps[fmiidx] = i
		fmiidx++
	}

	log.Printf("Creating indexes from %dKB...", size/1024)
	st := time.Now()
	fmi.Build(0, 1)
	log.Printf("done. (%fsecs)\n", time.Now().Sub(st).Seconds())

	of, err := os.Create("fts.index")
	if err != nil {
		log.Fatalln("Cannot save index:", err)
	}
	defer of.Close()

	st = time.Now()
	err = fmi.Write(of)
	if err != nil {
		log.Fatalln("Cannot save index:", err)
	}

	mf, err := os.Create("fts.map")
	if err != nil {
		panic(err)
	}
	defer mf.Close()

	err = codec.NewEncoder(mf, &codec.MsgpackHandle{}).Encode(&maps)
	if err != nil {
		panic(err)
	}
}
