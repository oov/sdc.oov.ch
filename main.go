package main

import (
	"bitbucket.org/oov/dgf"
	"flag"
	"fmt"
	"github.com/golang/glog"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"html/template"
	"math/rand"
	"net"
	"net/http"
	"net/http/fcgi"
	"os"
	"strings"
	"time"
)

var (
	df              *dgf.Dgf
	randomLyricChan chan *Lyric
	index           *template.Template
	tpl             = template.Must(template.ParseFiles(
		"tmpl/index.html",
	))
)

func generator() {
	maxId, err := df.GetLocalMaxId()
	if err != nil {
		panic(err)
	}
	if maxId == 0 {
		glog.Fatal("invalid local-max-id")
	}

	rng := rand.New(rand.NewSource(time.Now().UnixNano()))
	for {
		randomLyricChan <- NewLyric(df, rng.Int63())
	}
}

func getLyric(r *http.Request) *Lyric {
	var seed int64
	_, err := fmt.Sscanf(r.FormValue("r"), "%d", &seed)
	if err == nil {
		return NewLyric(df, seed)
	} else {
		return <-randomLyricChan
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	lyric := getLyric(r)
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	tpl.Execute(w, map[string]interface{}{"lyric": lyric})
}

func generateHandler(w http.ResponseWriter, r *http.Request) {
	lyric := getLyric(r)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(lyric.JSON())
	r.Body.Close()
}

var (
	fcgiserv = flag.String("fcgi", "tcp://127.0.0.1:10081", "fcgi server address")
	httpserv = flag.String("http", ":10080", "http server address")
)

func main() {
	flag.Parse()
	glog.Info("Starting...")

	db, err := leveldb.OpenFile("sdc.db", &opt.Options{})
	if err != nil {
		glog.Fatalln("Cannot open db:", err)
	}
	df = dgf.New(NewLevelDBAdapter(db))
	defer df.Close()

	randomLyricChan = make(chan *Lyric, 10)
	go generator()

	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/rlg", generateHandler)
	http.Handle("/r/", http.FileServer(http.Dir("pub/")))
	http.Handle("/favicon.ico", http.FileServer(http.Dir("pub/")))
	go func() {
		err = listenAndServe(*fcgiserv)
		if err != nil {
			glog.Fatalln("Cannot start fcgi server:", err)
		}
	}()
	err = http.ListenAndServe(*httpserv, nil)
	if err != nil {
		glog.Fatalln("Cannot start http server:", err)
	}
}

func listenAndServe(server string) (err error) {
	var s string
	var l net.Listener

	if strings.HasPrefix(server, "unix://") {
		s = (server)[7:]
		os.Remove(s)
		l, err = net.Listen("unix", s)
		os.Chmod(s, 0666)
	} else if strings.HasPrefix(server, "tcp://") {
		s = (server)[6:]
		l, err = net.Listen("tcp", s)
	} else {
		err = fmt.Errorf("unknown server bind address")
	}

	if err != nil {
		return
	}

	return fcgi.Serve(l, nil)
}
